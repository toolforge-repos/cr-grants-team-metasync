<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync\Tests;

use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use WikimediaCRGrantsTeam\Tool\MetaSync\Config;
use WikimediaCRGrantsTeam\Tool\MetaSync\ValueProcessor;

/**
 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\ValueProcessor
 */
class ValueProcessorTest extends TestCase {

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\ValueProcessor::processValue
	 * @dataProvider provideData
	 */
	public function testProcessValue( $varConfig, $value, $expected ) {
		$config = $this->createMock( Config::class );
		$config->method( 'getSyntaxConverters' )->willReturn( [
			'\#(http|https):\/\/(.*)\#' => '[$1://$2]'
		] );
		$processor = new ValueProcessor( $config->getSyntaxConverters() );

		$this->assertEquals( $expected, $processor->processValue( $varConfig, $value ) );
	}

	public function testProcessBlock() {
		$raw = file_get_contents( __DIR__ . '/../data/parseRaw.txt' );
		$expected = file_get_contents( __DIR__ . '/../data/parseFinal.txt' );

		$logger = $this->createMock( Logger::class );
		$config = new Config( __DIR__ . '/../data/validConfig.json', $logger );
		$processor = new ValueProcessor( $config->getSyntaxConverters() );
		$this->assertEquals( $expected, $processor->processValue( [ 'type' => 'text' ], $raw ) );
	}

	/**
	 * @return array
	 */
	public function provideData() {
		return [
			[ [ 'type' => 'ul' ], [ 'Value1', 'Value2', null ], "* Value1\n* Value2" ],
			[ [ 'type' => 'text' ], [ 'Value1', 'Value2', null ], "Value1, Value2" ],
			[ [ 'type' => 'bool' ], 1, 'Yes' ],
			[ [ 'type' => 'bool' ], '0', 'No' ],
			[ [ 'type' => 'bool' ], '1', 'Yes' ],
			[ [ 'type' => 'text' ], '#https://test.com#', '[https://test.com]' ],
		];
	}
}
