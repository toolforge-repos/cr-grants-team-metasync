<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync\Tests;

use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use WikimediaCRGrantsTeam\Tool\MetaSync\Config;
use WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate;
use WikimediaCRGrantsTeam\Tool\MetaSync\FluxxEntity;
use WikimediaCRGrantsTeam\Tool\MetaSync\ValueProcessor;

/**
 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate
 */
class EntityTemplateTest extends TestCase {
	/** @var EntityTemplate */
	private $template;
	/** @var FluxxEntity */
	private $entity;

	protected function setUp(): void {
		parent::setUp();

		$logger = $this->createMock( Logger::class );
		$config = new Config( __DIR__ . '/../data/validConfig.json', $logger );
		$this->template = new EntityTemplate(
			'dummy',
			file_get_contents( __DIR__ . '/../data/entityTemplateDummy.txt' ),
			'Dummy:Test/{project.title}',
			'dummy_type',
			[
				'dummy_field' => 'Dummy value',
			],
			[],
			new ValueProcessor( $config->getSyntaxConverters() )
		);

		$this->entity = new FluxxEntity( [
			'summary' => 'This is summary',
			'amount' => '22,34',
			'is_done' => '1',
			'a_list' => [ 'Value1', 'Value2' ],
			'created_at' => '01.11.2004',
			'title' => "Dummy: Entity",
			'parent.value' => 'value from relation',
			'project.title' => <<<HERE
Dummy: Entity
Some
HERE
		] );
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::getVariables
	 */
	public function testGetVariables() {
		$this->assertEquals( [
			'summary', 'amount', 'is_done', 'a_list', 'created_at', 'non_existing', 'dummy_field', 'model_document_ids'
		], $this->template->getVariables() );
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::getRelations
	 */
	public function testGetRelations() {
		$this->assertEquals( [
			'parent' => [ 'value' ],
			'project' => [ 'title' ],
		], $this->template->getRelations() );
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::getConditions
	 */
	public function testGetConditions() {
		$this->assertEquals(
			[ 'dummy_field' => 'Dummy value', ], $this->template->getConditions()
		);
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::getKey
	 */
	public function testGetKey() {
		$this->assertEquals(
			'dummy', $this->template->getKey()
		);
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::getEntityType
	 */
	public function testGetEntityType() {
		$this->assertEquals(
			'dummy_type', $this->template->getEntityType()
		);
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::getWikiPageName
	 */
	public function testGetWikiPageName() {
		$this->assertEquals(
			'Dummy:Test/Dummy:Entity Some',
			$this->template->getWikiPageName( $this->entity, new NullLogger() )
		);
	}

	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate::process
	 */
	public function testProcess() {
		$this->assertSame(
			file_get_contents( __DIR__ . '/../data/entityTemplateProcessed.txt' ),
			$this->template->process( $this->entity )
		);
	}
}
