<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync\Tests;

use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use WikimediaCRGrantsTeam\Tool\MetaSync\Config;

class ConfigTest extends TestCase {
	/**
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\Config::getWikiConnectionParams
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\Config::getFluxxConnectionParams
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\Config::get
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\Config::getWikiTemplate
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\Config::getWikiTemplateKeys
	 */
	public function testGet() {
		$logger = $this->createMock( Logger::class );
		$config = new Config( __DIR__ . '/../data/validConfig.json', $logger );

		$this->assertEquals( [
			'url' => 'http://127.0.0.1/api.php',
			'user' => 'wikitest',
			'password' => '5678'
		], $config->getWikiConnectionParams() );

		$this->assertEquals( [
			'url' => 'https://wmf.preprod.fluxxlabs.com',
			'client_id' => 'test',
			'client_secret' => '1234'
		], $config->getFluxxConnectionParams() );

		$this->assertEquals( './data', $config->get( 'data-dir' ) );

		$this->assertEquals( [
			'template_page' => 'MediaWiki:SyncTemplate_Dummy',
			'conditions' => [ 'dummy_field' => 'Dummy' ],
			'entity_type' => 'grant_request',
			'title_mask' => 'Grants:Programs/Wikimedia Community Fund/{project_title_long}'
		], $config->getWikiTemplate( 'dummy' ) );

		$this->assertEquals( [ 'dummy' ], $config->getWikiTemplateKeys() );
	}

	/**
	 * @param string $file
	 * @dataProvider provideInvalid
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\Config
	 */
	public function testConstructor( $file ) {
		$this->expectException( \InvalidArgumentException::class );
		$logger = new Logger( 'phpunit' );
		$config = new Config( $file, $logger );
	}

	/**
	 * @return array
	 */
	public function provideInvalid() {
		return [
			[ __DIR__ . '/../data/invalidConfig.json' ],
			[ 'invalidFileLocation.json' ]
		];
	}
}
