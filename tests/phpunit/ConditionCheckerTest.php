<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync\Tests;

use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use WikimediaCRGrantsTeam\Tool\MetaSync\ConditionChecker;
use WikimediaCRGrantsTeam\Tool\MetaSync\EntityTemplate;
use WikimediaCRGrantsTeam\Tool\MetaSync\FluxxEntity;

class ConditionCheckerTest extends TestCase {
	/**
	 * @param array $conditions
	 * @param bool $shouldMatch
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\ConditionChecker::check
	 * @dataProvider provideCheckers
	 */
	public function testCheck( $conditions, $shouldMatch ) {
		$entityMock = $this->createMock( FluxxEntity::class );
		$entityMock->method( 'get' )->willReturnCallback( static function ( $key ) {
			switch ( $key ) {
				case 'dummy.name':
					return 'test';
				case 'dummy.id':
					return 22;
				case 'foo':
					return 'bar';
				default:
					return '';
			}
		} );
		$templateMock = $this->createMock( EntityTemplate::class );
		$templateMock->method( 'getConditions' )->willReturn( $conditions );

		$loggerMock = $this->createMock( LoggerInterface::class );
		if ( !$shouldMatch ) {
			$loggerMock->expects( $this->once() )->method( 'info' );
		}

		$checker = new ConditionChecker( $entityMock, $templateMock, $loggerMock );
		$this->assertSame( $checker->check(), $shouldMatch );
	}

	/**
	 * @return array[]
	 */
	public function provideCheckers() {
		return [
			[
				[
					'dummy.name' => 'test',
					'foo' => 'bar'
				],
				true,
			],
			[
				[
					'include' => [
						'dummy.name' => 'test'
					],
					'exclude' => [
						'dummy.id' => 22,
						'foo' => 'bar'
					]
				],
				false,
			],
			[
				[
					'include' => [
						'dummy.name' => 'test'
					],
					'exclude' => [
						'dummy.id' => 22,
						'missing' => 'no match'
					]
				],
				true,
			],
			[
				[
					'include' => [
						'dummy.name' => 'test'
					],
				],
				true,
			],
		];
	}
}
