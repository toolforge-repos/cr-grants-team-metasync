<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync\Tests;

use PHPUnit\Framework\TestCase;
use WikimediaCRGrantsTeam\Tool\MetaSync\HtmlToWikitext;

class HtmlToWikitextTest extends TestCase {
	/**
	 * @param string $input
	 * @param bool $expected
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\HtmlToWikitext::isHtml
	 * @dataProvider provideIsHtml
	 */
	public function testIsHtml( string $input, bool $expected ) {
		$converter = new HtmlToWikitext();
		$this->assertSame( $expected, $converter->isHtml( $input ) );
	}

	/**
	 * @param string $input
	 * @param string $expected
	 * @return void
	 * @covers \WikimediaCRGrantsTeam\Tool\MetaSync\HtmlToWikitext::convert
	 * @dataProvider provideConvert
	 */
	public function testConvert( string $input, string $expected ) {
		$converter = new HtmlToWikitext();
		$this->assertSame( $expected, $converter->convert( $input ) );
	}

	/**
	 * @return array[]
	 */
	public function provideIsHtml() {
		return [
			[
				'<p>Foo</p>',
				true
			],
			[
				'Foo',
				false
			],
			[
				"<p>
					Foo
				</p>",
				true
			],
			[
				'This < that and that > this',
				false
			],
			[
				'<div>dummy',
				false
			]
		];
	}

	/**
	 * @return array[]
	 */
	public function provideConvert() {
		return [
			[
				'<h1 class="dummy">Heading 1</h1><p>Paragraph</p>',
				"=== Heading 1 ===

Paragraph"
			],
			[
				'<h2>Heading 2</h2><p>Paragraph</p>',
				"==== Heading 2 ====

Paragraph"
			],
			[
				file_get_contents( dirname( __DIR__ ) . '/data/rich.txt' ),
				file_get_contents( dirname( __DIR__ ) . '/data/rich_result.txt' ),
			]
		];
	}
}
