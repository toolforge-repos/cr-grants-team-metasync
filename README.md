# WMF CR Grant Team - MetaSync Tool

## Prerequisites

### Obtain API connection data from FLUXX (source)

1. Login to your app (https://wmf.fluxx.io)
2. Navigate to https://wmf.fluxx.io/oauth/applications
3. Click on 'New Application'
4. Add a name and redirect URL (just put the base URL)
5. Note the Application ID and Secret. Click on Authorize.
6. On the following page, click the Authorize button.

### Obtain API connection data for meta.wikimedia.org (target 1 - structured applications and reports)

1. Log into the wiki (https://meta.wikimedia.org)
2. Navigate to https://meta.wmf.labs.hallowelt.biz/w/index.php/Special:BotPasswords
3. Under "Create a new bot password", enter a bot name (e.g. `CRGrantTeamMetaSync`) and create the bot with
	1. "High-volume editing"
	2. "Edit existing pages"
	3. "Create, edit, and move pages"

See also https://www.mediawiki.org/wiki/Manual:Bot_passwords

### Obtain API connection data for Google Drive (target 2 - various associated files)

1. Log into the Google Developer Console (https://console.developers.google.com/)
2. Create a new "project" (e.g. `fluxx-drive-connector`), and navigate to its dashboard
3. Navigate to "Credentials" (https://console.cloud.google.com/apis/credentials) and choose "Service account" from the "+ Create credentials" menu
4. Provide a "service account name" (e.g. `cr-fluxx`) and grant access permissions to "Google Drive"
5. Click "Download JSON" button on in the Google Developer Console

## Setup on WMF Toolforge

1. Clone this codebase into a proper location: `git clone https://phabricator.wikimedia.org/source/tool-cr-grants-team-metasync.git`)
2. Install dependencies: `cd tool-cr-grants-team-metasync && composer install`
3. Copy `config-sample.json` to `config.json` and enter various API connection data obtained in "Prerequisites"
4. Set up a cron job to run the tool regularly: `crontab -e`

### Example crontab entry
This will run the tool every night at 0:00 o'clock UTC.
```
0 0 * * * /usr/bin/jsub -N cron-29 -once -quiet tool-cr-grants-team-metasync/bin/metasync sync --log-file=logs/tool.log --config=tool-cr-grants-team-metasync/config.json
```

# Concept and workflow

Workflow is as follows:
- Template is defined on the target wiki and specified in the config file
- When syncing, system will get template for the entity type being synced, look into placeholders and request
  those fields from FLUXX api.
- For each entity returned (since the last sync), it will replace placeholders with actual values and create a wikipage with resulting content


# Configuration

There are two parts to the configuration of the tool:
- Configuration on the server ("tool runner environment")
- Configuration on the wiki

For more detailed information about the configuration, please also check https://docs.google.com/document/d/1I1iFVk1dltzt26bChghHqwLBM2exDeXt6GxcwNmcLPo/edit

## Configuration on the server
This type of configuration specifies everything needed to establish connection with FLUXX API, the target wiki and the Google Drive API.
It holds all configuration that are not related to the content, so would not have to be changes very often.

This config is JSON file. See `config-sample.json` for an example. This file needs to be placed in tool's root, or be specified
as `--config` option when executing the `sync` command.

## Configuration on the wiki
This configuration is held on the target wiki, on the page referenced in the server-side config, in param `wiki-config-page`.
This page is a JSON page that holds all information about which entities to sync, which templates to use and how to name the pages.

Implementation is preferably done using `JsonConfig` extension, on a page in `Config` namespace, but alternatively could be any
JSON-contentmodel page on the target wiki.
In case `JsonConfig` is used, additional configuration on the target wiki is required.

```php
    $wgJsonConfigModels['JsonConfig.MetaSync'] = null;

    $wgJsonConfigs['JsonConfig.MetaSync'] = [
        'pattern' => '/^MetaSync$/',
    ];
```

This config holds information about the following items:

### Wiki-template specification

Templates are specified in `wiki-template` key of the config. Value of this key are key/value pairs of template ID (for internal purposed) and its specification:

```json
    "wiki-templates": {
        "dummy": {
            "conditions": {
                "project_title": "Dummy"
            },
            "entity_type": "grant_request",
            "template_page": "MediaWiki:SyncTemplate_Dummy",
            "title_mask": "Grants:Programs/Wikimedia Community Fund/{project_title_long}",
            "file_labels": [ "array", "of", "labels" ]
        }
    }
```

- `conditions` is an array of `fieldName => value` pairs that the template will use to make sure it should be used for given entity.
  In the example above, this template will ony be used for entities which have value of `project_title` = `Dummy. Condition value can also be an array. Condition will be met
  if field value matches any of the listed values.
    - Update:
    Now its possible to specify the negative conditions as well. In that case conditions should look like:
            
      
        {
               "include": [ /* Conditions, as before */ ],
               "exclude": [ /* Conditions, as before */ ]  
        }
      
- `entity_type` is the name of the FLUXX entity to retrieve
- `template_page` specifies the wikipage on target wiki where template is stored
- `title_mask` specifies the name of the final target page. This name can contain placeholders that refer to FLUXX entity fields (see example above). If FLUXX value for such fields is does not translate to a valid MW title, it will be normalized, as well as all `:` will be converted to `-`, to avoid unexpected NS issues.
In case field required to construct the page name does not exist in the retrieved FLUXX entity, sync for that entity will fail.
- `file_labels` specifies array of FLUXX file labels to allow upload of. All other files will be skipped

### Syntax converters
Due to differences between syntax in FLUXX and wiki, regex converters can be specified in the config file.

```json
    "syntax-converters": {
        "\\#(http|https):\\/\\/(.*)\\#": "[$1://$2]"
    }
```

Key should be the regex to match (properly escaped for JSON), while key is the replacement value.

# Template syntax
Template for the target wiki page is stored on the target wiki, and specified in the config file of the tool
This template is a wiki page containing the final content of the transfered page, with placeholders where the values
should be. Placeholders are defined like `$$flux_field$$`.
Placeholder can also contain the data type, and default value: `$$FLUXX_field;default_value;ul$$`. Supported types that can be specified as field types are: `ul` (for multi-fields) will create an unordered-list (default for multi-value fields is comma-separated list) and `bool` converts `1|0` to `Yes|No`

## Relational data
Template can also specified fields from related FLUXX entities. This is available for all FLUXX fields of type `relation`, and should be specified using dot notation: `$$parent_project.project_name`$$
This type of relational data specification is also available for `title_mask` syntax.

# Logging
Log file location needs to be specified in the command signature, using `--log-file` param. Default: `./tool-cr-grants-team-metasync.log`
This file needs to be writable!

# Email reporting
In server configuration, `error-email` key can be specified. This can be specified to a single email address, or an array of emails.
In case of any errors in the script execution, error report will be sent to these mails

# Data
Tool stores data about last sync and sync history. This is kept in a simple JSON file. Location of this file can be specified
in the config file. Default: `./data/syncdata.json`.
**This file needs to be writable!**
