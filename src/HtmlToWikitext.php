<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use DOMDocument;
use DOMXPath;

class HtmlToWikitext {

	/**
	 * @param string $value
	 * @return bool
	 */
	public function isHtml( string $value ) {
		$regex = "/<[^>]+>.*?<\/[^>]+>/s";
		return (bool)preg_match( $regex, $value );
	}

	/**
	 * @param string $value
	 * @return string
	 */
	public function convert( string $value ): string {
		// Remove line breaks between tags
		$value = preg_replace( "/>(\n+|\s+)</", '><', $value );
		$value = $this->convertLists( $value, false );
		// Strip `body` tag, leftover from parsing
		$value = preg_replace( "/<body.*?>(.*?)<\/body>/s", '$1', $value );
		$value = $this->convertHeadings( $value );
		$value = $this->convertParagraphs( $value );
		$value = $this->convertLinks( $value, true );

		return $this->removeSuperfluousNewlines( trim( $value ) );
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function convertHeadings( string $value ): string {
		$regex = "/<h([1-6]).*?>(.*?)<\/h[1-6]>/";
		return preg_replace_callback( $regex, static function ( $matches ) {
			$level = (int)$matches[1] + 2;
			if ( $level > 5 ) {
				$level = 5;
			}
			$text = trim( $matches[2] );
			if ( $text === '' ) {
				return '';
			}
			$prefix = str_repeat( '=', $level );
			return "$prefix $text $prefix\n\n";
		}, $value );
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function convertParagraphs( string $value, bool $addNewline = true ): string {
		$regex = "/<p(.*?)>(.*?)<\/p>/";
		return preg_replace_callback( $regex, function ( $matches ) use ( $addNewline ) {
			$text = $this->convertTextStylings( $matches[2] );
			$text = $this->convertLinks( $text );
			$attributes = $this->parseAttributes( $matches[1] );
			if ( isset( $attributes['class'] ) ) {
				switch ( $attributes['class'] ) {
					case 'text-center':
						return "<p style='text-align: center;'>$text</p>\n";
					case 'text-right':
						return "<p style='text-align: right;'>$text</p>\n";
				}
			}
			return $addNewline ? "$text\n\n" : $text;
		}, $value );
	}

	/**
	 * @param string $attributes
	 * @return array
	 */
	private function parseAttributes( string $attributes ) {
		// Match any xx="yy" or xx='yy' or xx=yy
		$regex = "/([a-z-]+)=[\"']?([a-z-]+)[\"']?/";
		$matches = [];
		preg_match_all( $regex, $attributes, $matches );
		$attributes = [];
		foreach ( $matches[1] as $i => $key ) {
			$attributes[$key] = trim( $matches[2][$i] );
		}
		return $attributes;
	}

	/**
	 * @param string $text
	 * @return string
	 */
	private function convertTextStylings( string $text ): string {
		$text = $this->convertLinks( $text );
		// Bold, italic, underline, superscript, subscript
		$replacements = [
			"/<strong.*?>(.*?)<\/strong>/" => "'''$1'''",
			"/<b.*?>(.*?)<\/b>/" => "'''$1'''",
			"/<em.*?>(.*?)<\/em>/" => "''$1''",
			"/<i.*?>(.*?)<\/i>/" => "''$1''",
			"/<u.*?>(.*?)<\/u>/" => "<u>$1</u>",
			"/<sup.*?>(.*?)<\/sup>/" => "<sup>$1</sup>",
			"/<sub.*?>(.*?)<\/sub>/" => "<sub>$1</sub>",
		];
		foreach ( $replacements as $regex => $replacement ) {
			$text = preg_replace( $regex, $replacement, $text );
		}
		return $text;
	}

	/**
	 * @param string $value
	 * @param bool $isSub
	 * @param array $processed
	 * @return array|string
	 */
	private function convertLists( string $value, bool $isSub, array &$processed = [] ) {
		// Load value to DOM document
		$doc = new DOMDocument( '1.0', 'UTF-8' );
		$value = mb_convert_encoding( $value, 'HTML-ENTITIES', 'UTF-8' );
		$doc->loadHTML( $value );
		// Find top level lists
		$xpath = new DOMXPath( $doc );
		$lists = $xpath->query(
			'//ol[not(ancestor::ol) and not(ancestor::ul)] | //ul[not(ancestor::ol) and not(ancestor::ul)]'
		);

		$allLines = [];
		foreach ( $lists as $list ) {
			if ( !$isSub ) {
				$processed = [];
			}
			$lines = [];
			$tag = $list->tagName;
			$items = $xpath->query( './/li', $list );
			// If there is another OL or UL in the LI, convert those
			foreach ( $items as $item ) {
				$listHtml = $doc->saveHTML( $item );
				if ( in_array( $listHtml, $processed ) ) {
					continue;
				}
				$processed[] = $listHtml;
				$lineText = [];
				foreach ( $item->childNodes as $child ) {
					if ( $child->nodeType === XML_TEXT_NODE ) {
						$lineText[] = $this->convertTextStylings( $child->textContent );
					} elseif ( $child->tagName !== 'ol' && $child->tagName !== 'ul' ) {
						$lineText[] = $this->convertParagraphs(
							$this->convertTextStylings( $doc->saveHTML( $child ) ),
							false
						);
					}
				}
				if ( $lineText ) {
					$lines[] = ( $tag === 'ol' ? '#' : '*' ) . ' ' . implode( '', $lineText );
				}

				if ( preg_match( '/<ol|<ul/', $listHtml ) ) {
					$sublist = $this->convertLists( $listHtml, true, $processed );
					foreach ( $sublist as $subline ) {
						$lines[] = ( $tag === 'ol' ? '#' : '*' ) . $subline;
					}
				}
			}
			if ( !$isSub ) {
				// Replace $list node with a text node containing the list
				$replacement = $doc->createTextNode( "\n" . implode( "\n", $lines ) . "\n\n" );
				$list->parentNode->replaceChild( $replacement, $list );
			} else {
				$allLines = array_merge( $allLines, $lines );
			}
		}
		return $isSub ? $allLines : $doc->saveHTML( $doc->getElementsByTagName( 'body' )->item( 0 ) );
	}

	/**
	 * @param string $text
	 * @param bool $addNewline
	 * @return string
	 */
	private function convertLinks( string $text, bool $addNewline = false ): string {
		$regex = "/<a[^>]*href=[\"'](.*?)[\"'][^>]*>(.*?)<\/a>/";
		return preg_replace_callback( $regex, static function ( $matches ) use ( $addNewline ) {
			if ( $matches[1] === $matches[2] ) {
				return "[$matches[1]]" . ( $addNewline ? "\n" : '' );
			}
			return "[$matches[1] $matches[2]]" . ( $addNewline ? "\n" : '' );
		}, $text );
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function removeSuperfluousNewlines( string $value ): string {
		// Remove superfluous newlines
		$value = preg_replace( "/\n{3,}/", "\n\n", $value );
		// Remove leading and trailing newlines
		return trim( $value );
	}
}
