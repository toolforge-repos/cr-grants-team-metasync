<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync\Command;

use Exception;
use InvalidArgumentException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use WikimediaCRGrantsTeam\Tool\MetaSync\Config;
use WikimediaCRGrantsTeam\Tool\MetaSync\ErrorReportMailer;
use WikimediaCRGrantsTeam\Tool\MetaSync\GoogleDriveClient;
use WikimediaCRGrantsTeam\Tool\MetaSync\Scheduler;
use WikimediaCRGrantsTeam\Tool\MetaSync\SyncLogger;
use WikimediaCRGrantsTeam\Tool\MetaSync\Updater;

class Sync extends Command {
	/** @var LoggerInterface */
	protected $logger = null;
	/** @var Config */
	protected $config = null;
	/** @var ErrorReportMailer */
	protected $mailer = null;
	/** @var Updater */
	private $updater = null;

	protected function configure() {
		$this
			->setName( 'sync' )
			->setDescription( 'Sync entities from Fluxx to wiki' )
			->setDefinition( new InputDefinition( [
				new InputOption(
					'config',
					null,
					InputOption::VALUE_OPTIONAL,
					'Specifies the config file to use',
					'./config.json'
				),
				new InputOption(
					'log-file',
					null,
					InputOption::VALUE_OPTIONAL,
					'Specifies the log file to use',
					'./tool-cr-grants-team-metasync.log'
				)
			] ) );
	}

	/**
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute( InputInterface $input, OutputInterface $output ) {
		if ( !$this->preflight( $input, $output ) ) {
			return 1;
		}

		$returnCode = 1;
		try {
			$output->writeln( 'Starting sync' );
			$this->updater->sync();
			$output->writeln( 'Complete' );
			$returnCode = 0;
		} catch ( Exception $ex ) {
			$output->writeln( 'Error:' . $ex->getMessage() );
			$this->getLogger()->critical( $ex->getMessage() );
		}

		// Send error report (if there were errors)
		$this->mailer->send();
		return $returnCode;
	}

	/**
	 * Run preflight procedure - init objects, assert all set
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return bool
	 */
	protected function preflight( InputInterface $input, OutputInterface $output ) {
		$output->writeln( 'Preflight checks...' );
		try {
			$this->initLogger( $input->getOption( 'log-file' ) );
			$this->initConfig( $input->getOption( 'config' ) );
			$this->initMailer( $this->config->get( 'error-email' ) );
			$drive = $this->initGoogleDrive();
			$scheduler = new Scheduler( realpath( $this->config->get( 'data-dir' ) ) );
		} catch ( Exception $ex ) {
			$output->writeln( $ex->getMessage() );
			return false;
		}
		// These two checks are more for sanity
		if ( !$this->getLogger() instanceof LoggerInterface ) {
			$output->writeln( 'Failed to initialize logger' );
			return false;
		}
		if ( !$this->getConfig() instanceof Config ) {
			$output->writeln( 'Failed to initialize Config object' );
			return false;
		}

		$this->updater = new Updater( $this->getConfig(), $this->getLogger(), $scheduler, $drive );

		return true;
	}

	/**
	 * @param string $configFile
	 */
	private function initConfig( string $configFile ) {
		$this->config = new Config( $configFile, $this->getLogger() );
		$this->config->loadWikiConfig();
		$this->config->validateIntegrity();
	}

	/**
	 * @param array|string $sendTo
	 */
	private function initMailer( $sendTo ) {
		if ( !is_array( $sendTo ) ) {
			$sendTo = [ $sendTo ];
		}
		$this->mailer = new ErrorReportMailer( $sendTo );
		$this->logger->setMailer( $this->mailer );
		$this->logger->info( 'Set mailer to ' . implode( ', ', $sendTo ) );
	}

	/**
	 * @param string $logFile
	 */
	private function initLogger( string $logFile ) {
		$logFile = realpath( $logFile );
		if ( !is_writable( $logFile ) ) {
			throw new InvalidArgumentException( 'Log file ' . $logFile . ' not writable' );
		}
		$this->logger = new SyncLogger( 'main' );
		$this->logger->pushHandler(
			new StreamHandler( $logFile, Logger::INFO )
		);
	}

	/**
	 * @return Logger
	 */
	protected function getLogger(): LoggerInterface {
		return $this->logger;
	}

	/**
	 * @return Config
	 */
	protected function getConfig(): Config {
		return $this->config;
	}

	/**
	 * @return GoogleDriveClient
	 * @throws \Google\Exception
	 */
	private function initGoogleDrive(): GoogleDriveClient {
		return new GoogleDriveClient( $this->config, $this->logger );
	}
}
