<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Google\Client;
use Google\Exception;
use Google\Service\Drive;
use Google\Service\Drive\DriveFile;
use Psr\Log\LoggerInterface;

class GoogleDriveClient {
	/** @var Client */
	private $client;
	/** @var Drive */
	private $drive;
	/** @var bool */
	private $tokenOK = false;
	/** @var string */
	private $parentDirID = '';

	/** @var Config */
	private $config;
	/** @var LoggerInterface */
	private $logger;

	/**
	 * @param Config $config
	 * @param LoggerInterface $logger
	 * @throws Exception
	 */
	public function __construct( Config $config, LoggerInterface $logger ) {
		$this->config = $config;
		$this->logger = $logger;
		$this->getClient();
	}

	/**
	 * @throws Exception
	 */
	private function getClient() {
		$this->client = new Client();
		$this->client->setApplicationName( 'fluxx-drive-connector' );
		$this->client->setScopes( Drive::DRIVE );
		$this->client->setAuthConfig( $this->config->getGoogleDriveConfig() );
		$accessToken = $this->client->fetchAccessTokenWithAssertion();
		if ( !$accessToken || !isset( $accessToken['access_token'] ) ) {
			$this->logger->error( 'Cannot retrieve access token for GoogleDrive upload', $accessToken );
		}
		$this->tokenOK = true;
		$this->drive = new Drive( $this->client );
	}

	/**
	 * @return string|null
	 * @throws Exception
	 */
	private function findParentDirID() {
		$parentDir = $this->config->getGoogleDriveConfig()['parentDir'] ?? null;
		if ( $parentDir === null ) {
			return null;
		}

		$list = $this->drive->files->listFiles(
			[ 'q' => 'mimeType=\'application/vnd.google-apps.folder\'' ]
		);
		foreach ( $list->getFiles() as $file ) {
			if ( $file->getName() === $parentDir ) {
				return $file->getId();
			}
		}

		$this->logger->warning( "GoogleDrive: Cannot find directory {$parentDir}, uploading to root!" );
		return null;
	}

	/**
	 * @return string
	 * @throws Exception
	 */
	private function getParentDirID() {
		if ( !$this->parentDirID ) {
			$this->parentDirID = $this->findParentDirID();
		}
		return $this->parentDirID;
	}

	/**
	 * @param File $file
	 * @return string|null
	 * @throws Exception
	 */
	public function uploadFile( File $file ) {
		if ( !$this->tokenOK ) {
			$this->logger->error(
				"GoogleDrive: Cannot upload {$file->getFilenameForDrive()}. Access token cannot be retrieved"
			);
			return null;
		}

		$fileId = $this->getFileId( $file );
		if ( $fileId !== null ) {
			return $this->updateFile( $fileId, $file );
		}
		$driveFile = $this->getDriveFile();
		$driveFile->setName( $file->getFilenameForDrive() );
		if ( $this->getParentDirID() ) {
			$driveFile->setParents( [ $this->getParentDirID() ] );
		}

		$res = $this->drive->files->create( $driveFile, [
			'data' => file_get_contents( $file->getPath() ),
			'fields' => [ 'id', 'webContentLink' ]
		] );
		return $this->postUpload( $res );
	}

	/**
	 * @param File $file
	 * @return string|null
	 * @throws Exception
	 */
	public function getFileId( File $file ) {
		$filename = $file->getFilenameForDrive();
		$filename = addslashes( $filename );
		$query = '';
		if ( $this->getParentDirID() ) {
			$query .= '\'' . $this->getParentDirID() . '\' in parents and ';
		}
		$query .= 'name=\'' . $filename . '\'';

		// Get file with this name in specified parent directory
		$list = $this->drive->files->listFiles(
			[ 'q' => $query ]
		);
		if ( empty( $list->getFiles() ) ) {
			return null;
		}
		$file = $list->getFiles()[0];
		return $file->getId();
	}

	/**
	 * @param string $fileId
	 * @param File $file
	 * @return string
	 * @throws \Google\Service\Exception
	 */
	private function updateFile( $fileId, File $file ) {
		$this->logger->info( 'File ' . $file->getFilenameForDrive() . ' already exists, updating...' );
		$driveFile = $this->getDriveFile();
		$res = $this->drive->files->update( $fileId, $driveFile, [
			'data' => file_get_contents( $file->getPath() ),
			'fields' => [ 'id', 'webContentLink' ]
		] );

		return $this->postUpload( $res );
	}

	/**
	 * @return DriveFile
	 */
	private function getDriveFile() {
		return new DriveFile( $this->client );
	}

	/**
	 * @param DriveFile $file
	 * @return string
	 */
	private function postUpload( DriveFile $file ) {
		$permission = new Drive\Permission();
		$permission->setRole( 'reader' );
		$permission->setType( 'anyone' );

		$this->drive->permissions->create( $file->getId(), $permission );

		$newFile = $this->drive->files->get( $file->getId(), [ 'fields' => [ 'webViewLink' ] ] );
		$this->logger->info( 'Upload to Google Drive complete', [
			'link' => $newFile->getWebViewLink()
		] );
		return $newFile->getWebViewLink();
	}
}
