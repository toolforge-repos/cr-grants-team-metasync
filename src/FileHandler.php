<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Psr\Log\LoggerInterface;

class FileHandler {
	/** @var LoggerInterface */
	private $logger;

	/**
	 * @param LoggerInterface $logger
	 */
	public function __construct( LoggerInterface $logger ) {
		$this->logger = $logger;
	}

	/**
	 * @param FluxxEntity $entity
	 * @param string $path
	 * @param string $grantId
	 * @return File
	 */
	public function getFileFromEntity( FluxxEntity $entity, $path, $grantId ): File {
		return new File(
			$path,
			$entity->get( 'document_file_name' ),
			$entity->get( 'document_description', '' ),
			$entity->get( 'api_url', $entity->get( 'url', '' ) ),
			$grantId
		);
	}
}
