<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

class ValueProcessor {
	/** @var array|null */
	private $syntaxConverters;

	/**
	 * @param array|null $syntaxConverters
	 */
	public function __construct( $syntaxConverters = [] ) {
		$this->syntaxConverters = $syntaxConverters;
	}

	/**
	 * @param array $valueConfig
	 * @param mixed $value
	 * @return string
	 */
	public function processValue( array $valueConfig, $value ) {
		$type = $valueConfig['type'];
		if ( is_array( $value ) ) {
			return $this->processArrayValue( $value, $type );
		}

		switch ( $type ) {
			case 'bool':
				return $this->processBoolValue( $value );
			default:
				return $this->convertSyntax( $value );
		}
	}

	/**
	 * @param array $value
	 * @param string $type
	 * @return string
	 */
	private function processArrayValue( array $value, string $type ): string {
		// Filter out nulls
		$value = array_filter( $value, static function ( $bit ) {
			return $bit !== null;
		} );

		if ( $type === 'ul' ) {
			return $this->makeUnorderedList( $value );
		}

		return implode( ', ', $value );
	}

	/**
	 * @param array $value
	 * @return string
	 */
	private function makeUnorderedList( array $value ): string {
		$value = array_map( function ( $singleValue ) {
			return '* ' . $this->convertSyntax( $singleValue );
		}, $value );

		if ( count( $value ) === 1 ) {
			return $value[0];
		}
		return implode( "\n", $value );
	}

	/**
	 * @param string|int $value
	 * @return string
	 */
	private function processBoolValue( $value ): string {
		return (bool)$value ? 'Yes' : 'No';
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function convertSyntax( $value ) {
		$value = (string)$value;
		$converter = new HtmlToWikitext();
		if ( $converter->isHtml( $value ) ) {
			return $converter->convert( $value );
		}
		foreach ( $this->syntaxConverters as $regex => $replacement ) {
			if ( substr( $regex, 0, 1 ) !== '/' ) {
				$regex = '/' . $regex . '/';
			}
			$replacement = stripslashes( $replacement );
			if ( preg_match( $regex, $value ) ) {
				$value = preg_replace( $regex, $replacement, $value );
			}
		}
		return trim( $value );
	}
}
