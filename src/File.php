<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

class File {
	/** @var string */
	private $path;
	/** @var string */
	private $filename;
	/** @var string */
	private $description;
	/** @var string */
	private $source;
	/** @var string */
	private $grantId;

	/**
	 * @param string $path
	 * @param string $filename
	 * @param string $description
	 * @param string $source
	 * @param string $grantId
	 */
	public function __construct( $path, $filename, $description, $source, $grantId ) {
		$this->path = $path;
		$filename = str_replace( '[', '(', $filename );
		$filename = str_replace( ']', ')', $filename );
		$this->filename = $filename;
		$this->description = $description;
		$this->source = $source;
		$this->grantId = $grantId;
	}

	/**
	 * @return string
	 */
	public function getPath(): string {
		return $this->path;
	}

	/**
	 * @return string
	 */
	public function getFilename(): string {
		return $this->filename;
	}

	/**
	 * @return string
	 */
	public function getFilenameForDrive(): string {
		// Make sure no two files (in different grants) have the same name
		return $this->grantId . '-' . $this->filename;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getSource(): string {
		return $this->source;
	}

	/**
	 * Just used when uploading files directly to wiki
	 * @return string
	 */
	public function getFilePageContent(): string {
		return sprintf(
			"{{Information
			|description=%s
			|source=%s
			|author=%s
			}}",
			$this->getDescription(),
			$this->getSource(),
			// TODO: Better name
			'MetaFluxx sync tool'
		);
	}
}
