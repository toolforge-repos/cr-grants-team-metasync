<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use PHPMailer\PHPMailer\PHPMailer;
use Psr\Log\LoggerInterface;

class ErrorReportMailer {
	/** @var array */
	private $to;
	/** @var array */
	private $errors = [];
	/** @var LoggerInterface */
	private $logger;

	/**
	 * @param array $to
	 */
	public function __construct( array $to ) {
		$this->to = $to;
	}

	/**
	 * @param string $message
	 * @param array $context
	 */
	public function addError( $message, $context = [] ) {
		$this->errors[] = [
			'message' => $message,
			'context' => $context,
		];
	}

	/**
	 * @param LoggerInterface $logger
	 */
	public function setLogger( $logger ) {
		$this->logger = $logger;
	}

	/**
	 * Send error report
	 */
	public function send() {
		if ( empty( $this->errors ) ) {
			if ( $this->logger ) {
				$this->logger->info( 'Error report: No errors' );
			}
			return;
		}
		if ( $this->logger ) {
			$this->logger->info( 'Error report: sending', [
				'emails' => $this->to,
			] );
		}
		$body = $this->compose();
		foreach ( $this->to as $recipient ) {
			$this->logger->info( 'Sending error report to ' . $recipient );
			$mail = new PHPMailer();
			$mail->isSMTP();
			$mail->Host = 'mail.tools.wmflabs.org';
			$mail->SMTPAuth = false;
			$mail->Port = 25;

			$mail->setFrom( 'tools.cr-grants-team-metasync@tools.wmflabs.org' );
			$mail->addAddress( $recipient );
			$mail->isHTML( false );
			$mail->Subject = 'Meta sync tool error report';
			$mail->Body = $body;

			$res = $mail->send();
			$this->logger->info( 'Mail send result: ' . ( $res ? 'success' : 'fail' ) );
		}
	}

	/**
	 * @return string
	 */
	private function compose() {
		$message = [];
		foreach ( $this->errors as $error ) {
			$message[] = $error['message'];
			if ( !empty( $error['context'] ) ) {
				// phpcs:ignore MediaWiki.WhiteSpace.SpaceBeforeSingleLineComment.NewLineComment
				$message[] = json_encode( $error['context'], JSON_PRETTY_PRINT );
			}
			$message[] = '----------------------';
		}

		return implode( "\n", $message );
	}
}
