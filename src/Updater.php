<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Throwable;
use WikimediaCRGrantsTeam\Tool\MetaSync\Api\FluxxApi;
use WikimediaCRGrantsTeam\Tool\MetaSync\Api\WikiApi;

class Updater {
	/** @var Config */
	private $config;
	/** @var LoggerInterface */
	private $logger;
	/** @var Scheduler */
	private $scheduler;
	/** @var GoogleDriveClient */
	private $drive;
	/** @var array */
	private $templateCache = [];

	/** @var WikiApi */
	private $wikiApi;
	/** @var FluxxApi */
	private $fluxxApi;
	/** @var FileHandler */
	private $fileHandler;

	/**
	 * @param Config $config
	 * @param LoggerInterface $logger
	 * @param Scheduler $scheduler
	 * @param GoogleDriveClient $drive
	 */
	public function __construct(
		Config $config, LoggerInterface $logger, Scheduler $scheduler, GoogleDriveClient $drive
	) {
		$this->config = $config;
		$this->logger = $logger;
		$this->scheduler = $scheduler;
		$this->drive = $drive;

		$this->wikiApi = new WikiApi( $config->getWikiConnectionParams(), $logger );
		$this->fluxxApi = new FluxxApi( $config->getFluxxConnectionParams(), $logger );
		$this->fileHandler = new FileHandler( $logger );
	}

	/**
	 * Execute sync
	 * @throws Exception
	 */
	public function sync() {
		$this->syncRetries();
		$this->syncEntities();

		$this->scheduler->recordSync();
	}

	private function syncEntities() {
		$processed = [];
		foreach ( $this->getTemplates() as $template ) {
			$entities = $this->fluxxApi->getEntities(
				$template->getEntityType(), $template->getVariables(),
				$template->getRelations(), $this->scheduler->getLast()
			);
			if ( $entities === null ) {
				$this->logger->error( 'Failed to retrieve entities for template ' . $template->getKey() );
				continue;
			}
			foreach ( $entities as $entity ) {
				$this->logger->info( 'Testing template ' . $template->getKey() . ' for ' . $entity->getId() );
				if ( !$this->templateMatchesEntity( $entity, $template ) ) {
					$this->logger->info( "Template does not match" );
					continue;
				}
				$this->logger->info( "Going with this template" );
				if ( in_array( $entity->getId(), $processed ) ) {
					$this->logger->warning(
						'Same entity processed for two templates. This may indicate config issue',
						$entity->getAll()
					);
					continue;
				}
				$res = $this->processEntity( $entity, $template );
				if ( $res ) {
					$processed[] = $entity->getId();
				}
			}
		}
	}

	/**
	 * @param FluxxEntity $entity
	 * @param EntityTemplate $template
	 * @return bool
	 * @throws GuzzleException
	 */
	private function processEntity( FluxxEntity $entity, EntityTemplate $template ) {
		if ( !$this->templateMatchesEntity( $entity, $template ) ) {
			return false;
		}
		$wikiPageName = $template->getWikiPageName( $entity, $this->logger );
		if ( !$wikiPageName ) {
			$this->tryScheduleRetry( $entity, $template );
			return false;
		}
		$this->logger->info( '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>' );
		$this->logger->info(
			"Start processing entity of type {$template->getEntityType()}, id: {$entity->getId()}"
		);

		$files = $this->uploadDocumentsForEntity( $entity, $template->getFileLabels() );
		$text = $template->process( $entity, $files );
		$result = $this->wikiApi->editPage( $wikiPageName, $text );
		if ( !$result ) {
			$this->logger->critical(
				"Could not sync entity of type {$template->getEntityType()}, id: {$entity->getId()}",
				$entity->getAll()
			);
			$this->tryScheduleRetry( $entity, $template );
		}
		$this->logger->info(
			"Synced entity of type {$template->getEntityType()}, id:  {$entity->getId()}  to $wikiPageName"
		);
		$this->logger->info( '<<<<<<<<<<<<<<<<<<<<<<<<<<<<' );

		return false;
	}

	/**
	 * @param FluxxEntity $entity
	 * @param array $fileLabels
	 * @throws GuzzleException
	 * @return array
	 */
	private function uploadDocumentsForEntity( FluxxEntity $entity, $fileLabels ) {
		$documentIds = $entity->get( 'model_document_ids', [] );
		if ( is_numeric( $documentIds ) ) {
			$documentIds = [ $documentIds ];
		}
		if ( !is_array( $documentIds ) ) {
			return [];
		}

		$files = [];
		foreach ( $documentIds as $documentId ) {
			$this->processDocumentUpload( $entity, $fileLabels, $documentId, $files );
		}
		return $files;
	}

	/**
	 * @param FluxxEntity $entity
	 * @param array $fileLabels
	 * @param string|int $documentId
	 * @param array &$files
	 * @throws GuzzleException
	 * @return void
	 */
	private function processDocumentUpload( FluxxEntity $entity, $fileLabels, $documentId, &$files ) {
		// Get document based on ID referenced in grant
		$documentEntity = $this->fluxxApi->getEntity(
			$documentId, 'model_document', [
			'document_file_name', 'model_document_type_id', 'document_description'
		], [ 'model_document_type_id' => [ 'name' ] ] );
		if ( !$documentEntity || !$documentEntity->get( 'document_file_name' ) ) {
			$this->logger->error( "Could not retrieve document details", [
				'entity' => [ 'id' => $entity->getId(), 'project_title' => $entity->get( 'project_title' ) ],
				'document_id' => $documentId
			] );
			return;
		}

		$documentFilename = $documentEntity->get( 'document_file_name' );
		if ( !$this->shouldAllowFile( $documentEntity, $fileLabels ) ) {
			return;
		}

		$this->logger->info( 'Starting upload of document ' . $documentFilename );
		// Download document to temp location
		$tempLocation = $this->config->get( 'data-dir' ) . '/' . $entity->getId() . '-' . $documentFilename;
		$downloadRes = $this->fluxxApi->downloadFile( $documentId, $tempLocation );
		if ( !$downloadRes ) {
			return;
		}
		$file = $this->fileHandler->getFileFromEntity( $documentEntity, $tempLocation, $entity->getId() );
		try {
			$fileLink = $this->drive->uploadFile( $file );
			if ( $fileLink === null ) {
				throw new Exception();
			}
		} catch ( \Google\Exception | Exception $ex ) {
			$this->logger->error( "Failed to upload file to Google Drive: " . $ex->getMessage() );
			return;
		}

		$type = $this->getDocumentTypeLabel( $documentEntity );
		if ( !isset( $files[$type] ) ) {
			$files[$type] = [];
		}
		$files[$type][] = [
			'name' => $file->getFilename(),
			'link' => $fileLink
		];
		unlink( $tempLocation );
	}

	private function syncRetries() {
		$retries = $this->scheduler->getEntitiesToRetry( $this->config->getRetryAttemptLimit() );
		foreach ( $retries as $id => $data ) {
			$template = $this->getSingleTemplate( $data['templateKey'] );
			if ( !$template ) {
				$this->logger->error( 'Failed to retrieve template for retry: ' . $data['templateKey'] );
				$this->scheduler->giveUp( $id );
				continue;
			}
			$entity = $this->fluxxApi->getEntity(
				$id, $template->getEntityType(), $template->getVariables(), $template->getRelations()
			);
			if ( !$entity ) {
				$this->logger->error(
					"Retry sync of entity id: $id failed, cannot retrieve entity data"
				);
				$this->scheduler->giveUp( $id );
				continue;
			}
			$this->processEntity( $entity, $template );
		}
	}

	/**
	 * Check if this template matches entity data passed
	 *
	 * @param FluxxEntity $entity
	 * @param EntityTemplate $template
	 * @return bool
	 */
	private function templateMatchesEntity( FluxxEntity $entity, EntityTemplate $template ): bool {
		$checker = new ConditionChecker( $entity, $template, $this->logger );
		return $checker->check();
	}

	/**
	 * @param FluxxEntity $entity
	 * @param EntityTemplate $template
	 */
	private function tryScheduleRetry( FluxxEntity $entity, EntityTemplate $template ) {
		$attempts = $this->scheduler->getAttempts( $entity );
		if ( $attempts >= $this->config->getRetryAttemptLimit() ) {
			$this->logger->critical(
				"Retry limit reached for entity type {$template->getEntityType()}, id: {$entity->getId()}"
			);
			$this->scheduler->removeRetryFlag( $entity );
			return;
		}
		$this->scheduler->scheduleRetry( $entity, $template );
	}

	/**
	 * @return EntityTemplate[]
	 * @throws GuzzleException
	 */
	private function getTemplates(): array {
		$templateKeys = $this->config->getWikiTemplateKeys();
		$templates = [];
		foreach ( $templateKeys as $key ) {
			try {
				$template = $this->getSingleTemplate( $key );
				if ( !$template ) {
					throw new Exception( "Template not found" );
				}
			} catch ( Throwable $ex ) {
				$this->logger->error( "Failed to retrieve template $key: " . $ex->getMessage() );
				continue;
			}
			$templates[] = $template;
		}

		return $templates;
	}

	/**
	 * @param string $name
	 * @return EntityTemplate|null
	 * @throws GuzzleException
	 */
	private function getSingleTemplate( $name ): ?EntityTemplate {
		if ( isset( $this->templateCache[$name] ) ) {
			return $this->templateCache[$name];
		}
		$templateData = $this->config->getWikiTemplate( $name );
		if ( !$templateData ) {
			return null;
		}
		if ( !$this->verifyTemplateData( $templateData, $name ) ) {
			return null;
		}

		$templatePage = $templateData['template_page'];
		$templateContent = $this->wikiApi->retrieveTemplateContent( $templatePage );
		$template = new EntityTemplate(
			$name,
			$templateContent,
			$templateData['title_mask'],
			$templateData['entity_type'],
			$templateData['conditions'] ?? [],
			$templateData['file_labels'] ?? [],
			new ValueProcessor( $this->config->getSyntaxConverters() )
		);
		$this->templateCache[$name] = $template;
		return $template;
	}

	/**
	 * @param array $templateData
	 * @param string $key
	 * @return bool
	 */
	private function verifyTemplateData( array $templateData, string $key ): bool {
		if ( !is_array( $templateData ) ) {
			$this->logger->error( "Template $key not properly configured" );
			return false;
		}
		if ( !isset( $templateData['template_page'] ) || !$templateData['template_page'] ) {
			$this->logger->error( "Template $key does not contain \"template_page\" param" );
			return false;
		}

		if ( !isset( $templateData['title_mask'] ) || !$templateData['title_mask'] ) {
			$this->logger->error( "Template $key does not contain \"title_mask\" param" );
			return false;
		}

		if ( !isset( $templateData['entity_type'] ) || !$templateData['entity_type'] ) {
			$this->logger->error( "Template $key does not contain \"entity_type\" param" );
			return false;
		}

		return true;
	}

	/**
	 * @param FluxxEntity $document
	 * @param array $fileLabels
	 * @return bool
	 */
	private function shouldAllowFile( FluxxEntity $document, array $fileLabels ): bool {
		if ( empty( $fileLabels ) ) {
			return true;
		}
		$type = $this->getDocumentTypeLabel( $document );
		if ( $type !== null ) {
			foreach ( $fileLabels as $label ) {
				if ( $type === $label ) {
					return true;
				}
			}
		}

		$this->logger->info(
			'Skipped upload of file ' . $document->get( 'document_file_name' ) . ': File not allowed by config'
		);
		return false;
	}

	/**
	 * @param FluxxEntity $documentEntity
	 *
	 * @return string
	 */
	private function getDocumentTypeLabel( $documentEntity ) {
		return trim( $documentEntity->get( 'model_document_type_id.name', '' ) );
	}
}
