<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use DateTime;
use DateTimeZone;
use Exception;

class Scheduler {
	/** @var string */
	private $dataFile;
	/** @var array|null */
	private $data = null;

	/**
	 * @param string $dataDir
	 * @throws Exception
	 */
	public function __construct( string $dataDir ) {
		$this->dataFile = $dataDir . '/syncdata.json';

		if ( !is_writable( $this->dataFile ) ) {
			throw new Exception( 'Data file ' . $this->dataFile . ' is not writable!' );
		}
	}

	/**
	 * DateTime of last sync, or now if no previous sync recorded
	 * @return DateTime
	 * @throws Exception
	 */
	public function getLast(): DateTime {
		$this->loadData();
		if ( empty( $this->data ) ) {
			return new DateTime( 'now', new DateTimeZone( 'UTC' ) );
		}
		if ( !isset( $this->data['last'] ) ) {
			return new DateTime( 'now', new DateTimeZone( 'UTC' ) );
		}

		return DateTime::createFromFormat( 'Y-m-d H:i:s', $this->data['last'], new DateTimeZone( 'UTC' ) );
	}

	/**
	 * Schedule entity for a retry
	 *
	 * @param FluxxEntity $entity
	 * @param EntityTemplate $template
	 */
	public function scheduleRetry( FluxxEntity $entity, EntityTemplate $template ) {
		$this->loadData();
		if ( !isset( $this->data['retry'] ) ) {
			$this->data['retry'] = [];
		}
		$id = $entity->getId();
		if ( isset( $this->data['retry'][$id] ) ) {
			$this->data['retry'][$id]['attempts'] += 1;
		} else {
			$this->data['retry'][$id] = [
				'templateKey' => $template->getKey(),
				'attempts' => 0
			];
		}
		$this->persist();
	}

	/**
	 * Retry attempt count
	 *
	 * @param FluxxEntity $entity
	 * @return int
	 */
	public function getAttempts( FluxxEntity $entity ) {
		$this->loadData();
		if ( isset( $this->data['retry'][$entity->getId()]['attempts'] ) ) {
			return $this->data['retry'][$entity->getId()]['attempts'];
		}

		return 0;
	}

	/**
	 * @param FluxxEntity $entity
	 */
	public function removeRetryFlag( FluxxEntity $entity ) {
		$this->loadData();
		if ( isset( $this->data['retry'][$entity->getId()] ) ) {
			unset( $this->data['retry'][$entity->getId()] );
			$this->persist();
		}
	}

	/**
	 * Get all entities that need retry
	 *
	 * @param int $maxAttempts
	 *
	 * @return array
	 */
	public function getEntitiesToRetry( int $maxAttempts ): array {
		$this->loadData();
		$entities = $this->data['retry'] ?? [];
		return array_filter( $entities, static function ( $entity ) use ( $maxAttempts ) {
			return $entity['attempts'] < $maxAttempts;
		} );
	}

	/**
	 * Record synchronization
	 *
	 */
	public function recordSync() {
		$this->loadData();
		$now = new DateTime( 'now', new DateTimeZone( 'UTC' ) );
		$this->data['last'] = $now->format( 'Y-m-d H:i:s' );
		$this->persist();
	}

	/**
	 * @param string|int $id
	 * @return void
	 */
	public function giveUp( $id ) {
		$this->loadData();
		if ( isset( $this->data['retry'][$id] ) ) {
			unset( $this->data['retry'][$id] );
			$this->persist();
		}
	}

	private function persist() {
		file_put_contents( $this->dataFile, json_encode( $this->data, JSON_PRETTY_PRINT ) );
	}

	/**
	 * @return array
	 */
	private function loadData(): array {
		if ( $this->data === null ) {
			$this->data = [];
			if ( !file_exists( $this->dataFile ) ) {
				return $this->data;
			}
			$data = json_decode( file_get_contents( $this->dataFile ), 1 );
			if ( is_array( $data ) ) {
				$this->data = $data;
			}
		}

		return $this->data;
	}
}
