<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

class FluxxEntity {
	/** @var array */
	private $data;

	/**
	 * @param array $data
	 */
	public function __construct( array $data ) {
		$this->data = $this->parseFlat( $data );
	}

	/**
	 * @return string
	 */
	public function getId(): string {
		return $this->get( 'id' );
	}

	/**
	 * @param string $key
	 * @param string $default
	 * @return mixed|string
	 */
	public function get( $key, $default = '' ) {
		return $this->data[$key] ?? $default;
	}

	/**
	 * @return array
	 */
	public function getAll(): array {
		return $this->data;
	}

	/**
	 * @param array $data
	 * @return array
	 */
	private function parseFlat( array $data ) {
		$flattened = [];
		foreach ( $data as $field => $value ) {
			if ( !is_array( $value ) ) {
				$flattened[$field] = $value;
				continue;
			}
			if ( count( $value ) === count( $value, COUNT_RECURSIVE ) ) {
				// multi-value
				$arrayValue = array_filter( $value, static function ( $valueBit ) {
					return $valueBit;
				} );
				if ( count( $arrayValue ) === 1 ) {
					$flattened[$field] = $value;
				} else {
					$flattened[$field] = $value;
				}
				continue;
			}
			// nested - relation
			foreach ( $value as $valueItem ) {
				foreach ( $valueItem as $dataKey => $dataValue ) {
					$flattened["$field.$dataKey"] = $dataValue;
				}
				break;
			}
		}

		return $flattened;
	}
}
