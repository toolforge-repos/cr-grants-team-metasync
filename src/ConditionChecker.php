<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Psr\Log\LoggerInterface;

class ConditionChecker {
	/** @var FluxxEntity */
	private $entity;
	/** @var EntityTemplate */
	private $template;
	/** @var LoggerInterface */
	private $logger;

	/**
	 * @param FluxxEntity $entity
	 * @param EntityTemplate $template
	 * @param LoggerInterface $logger
	 */
	public function __construct(
		FluxxEntity $entity, EntityTemplate $template, LoggerInterface $logger
	) {
		$this->entity = $entity;
		$this->template = $template;
		$this->logger = $logger;
	}

	/**
	 * @return bool
	 */
	public function check(): bool {
		$conditions = $this->template->getConditions();
		if ( !$conditions ) {
			return true;
		}

		if ( !isset( $conditions['include'] ) ) {
			$conditions['include'] = $conditions;
		}

		if ( $this->matches( $conditions['include'] ) ) {
			if ( !isset( $conditions['exclude'] ) ) {
				return true;
			}
			$notExcluded = $this->matchesReverse( $conditions['exclude'] );
			if ( !$notExcluded ) {
				$this->logger->info( "Entity " . $this->entity->getId() . ' skipped', [
					'reason' => 'Excluded by conditions',
					'conditions' => json_encode( $conditions['exclude'] ),
					'values' => $this->getConditionValue( $conditions['exclude'] )
				] );
			}
			return $notExcluded;
		}

		$this->logger->info( "Entity " . $this->entity->getId() . ' skipped', [
			'reason' => 'Not included by conditions',
			'conditions' => json_encode( $conditions['include'] ),
			'values' => $this->getConditionValue( $conditions['include'] )
		] );

		return false;
	}

	/**
	 * @param array $conditions
	 * @return bool
	 */
	private function matches( array $conditions ) {
		foreach ( $conditions as $field => $value ) {
			$fieldValue = $this->entity->get( $field );
			if ( is_array( $value ) ) {
				if ( !in_array( $fieldValue, $value ) ) {
					return false;
				}
			}

			if ( is_bool( $value ) ) {
				$valuesToCheck = $value ?
					[ true, 1, '1', 'yes', 'YES', 'Yes' ] :
					[ false, 0, '0', 'no', 'NO', 'No' ];
				if ( !in_array( $fieldValue, $valuesToCheck ) ) {
					return false;
				}
			} elseif ( $fieldValue !== $value ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param array $conditions
	 * @return bool
	 */
	private function matchesReverse( array $conditions ) {
		foreach ( $conditions as $field => $value ) {
			$fieldValue = $this->entity->get( $field, 'N/S' );
			if ( $fieldValue === 'N/S' ) {
				// If field is not set, condition set does not apply
				return true;
			}
			if ( is_array( $value ) ) {
				if ( !in_array( $fieldValue, $value ) ) {
					return true;
				}
			}
			if ( is_bool( $value ) ) {
				$valuesToCheck = $value ?
					[ true, 1, '1', 'yes', 'YES', 'Yes' ] :
					[ '', false, 0, '0', 'no', 'NO', 'No' ];
				if ( !in_array( $fieldValue, $valuesToCheck ) ) {
					return true;
				}
			} elseif ( $fieldValue !== $value ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @param array $conditions
	 * @return false|string
	 */
	private function getConditionValue( array $conditions ) {
		$res = [];
		foreach ( $conditions as $field => $value ) {
			$res[$field] = $this->entity->get( $field );
		}

		return json_encode( $res );
	}
}
