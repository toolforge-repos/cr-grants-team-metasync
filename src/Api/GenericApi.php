<?php
namespace WikimediaCRGrantsTeam\Tool\MetaSync\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

abstract class GenericApi {
	/** @var LoggerInterface */
	private $logger;
	/** @var Client */
	private $client;
	/** @var CookieJar */
	private $cookieJar;
	/** @var array */
	protected $connectionConfig;

	/**
	 * @param array $connectionConfig
	 * @param LoggerInterface $logger
	 */
	public function __construct( array $connectionConfig, LoggerInterface $logger ) {
		$this->logger = $logger;
		$this->cookieJar = new CookieJar();
		$this->client = new Client( [ 'cookies' => $this->cookieJar ] );
		$this->connectionConfig = $connectionConfig;
	}

	/**
	 * @return void
	 */
	abstract protected function authenticate();

	/**
	 * @param string $method
	 * @param string $path
	 * @param array|null $data
	 * @param array|null $options
	 * @return ResponseInterface
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	abstract protected function runAuthenticatedRequest( $method, $path, $data = [], $options = [] ): ResponseInterface;

	/**
	 * @param string $path
	 * @return string
	 */
	protected function getUrl( $path = '' ): string {
		$base = $this->connectionConfig['url'];
		$base = rtrim( $base, '/' );
		if ( !$path ) {
			return $base;
		}
		$path = ltrim( $path, '/' );
		if ( strpos( $path, '?' ) === 0 ) {
			return $base . $path;
		}
		return $base . '/' . $path;
	}

	/**
	 * @param string $method
	 * @param string $path
	 * @param array|null $options
	 * @return ResponseInterface
	 */
	protected function request( $method, $path, $options = [] ): ResponseInterface {
		$url = $this->getUrl( $path );

		try {
			$response = $this->client->request( $method, $url, $options );
		} catch ( GuzzleException $ex ) {
			return new Response( 500, [], $ex->getMessage() );
		}

		if ( $response->getStatusCode() !== 200 ) {
			$this->logger->error( 'Request to URL {url} failed!', [
				'url' => $url,
			] );
		}

		return $response;
	}

	/**
	 * Compile params into URL
	 *
	 * @param string $path
	 * @param array $data
	 * @return string
	 */
	protected function compileURL( $path, array $data ): string {
		$params = [];
		foreach ( $data as $key => $value ) {
			if ( !is_string( $value ) ) {
				$value = json_encode( $value );
			}
			$params[] = "$key=$value";
		}

		$path = rtrim( $path, '/' );
		$separator = strpos( $path, '?' ) !== false ? '&' : '?';
		$url = $path . $separator . implode( '&', $params );
		$this->getLogger()->debug( 'Compiled URL', [
			'url' => $url
		] );
		return $url;
	}

	/**
	 * @return LoggerInterface
	 */
	protected function getLogger(): LoggerInterface {
		return $this->logger;
	}
}
