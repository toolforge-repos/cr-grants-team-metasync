<?php
namespace WikimediaCRGrantsTeam\Tool\MetaSync\Api;

use DateTime;
use Psr\Http\Message\ResponseInterface;
use WikimediaCRGrantsTeam\Tool\MetaSync\FluxxEntity;

class FluxxApi extends GenericApi {
	/** @var string */
	private $accessToken = null;

	/**
	 * @var string[]
	 */
	private $entityEndpointMap = [
		'grant_request' => [
			'endpoint' => 'grant_request/list',
			'qualifying_field' => 'updated_at',
		],
		'model_document' => [
			'endpoint' => 'model_document/list',
			'qualifying_field' => 'document_updated_at',
		],
		'request_report' => [
			'endpoint' => 'request_report/list',
			'qualifying_field' => 'updated_at',
		]
	];

	/**
	 * @param string $id
	 * @param string $type
	 * @param array $fields
	 * @param array $relations
	 * @return FluxxEntity|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function getEntity( string $id, string $type, array $fields, array $relations ): ?FluxxEntity {
		$entities = [];
		$status = $this->retrieveRecursively( [
			'cols' => json_encode( $fields ),
			'relation' => json_encode( $relations ),
			'filter' => json_encode( [
				[ 'id', 'eq', $id ],
			] ),
		], $this->getEndpointForEntityType( $type ), $type, $entities );

		if ( !$status || empty( $entities ) ) {
			return null;
		}

		return $entities[0];
	}

	/**
	 * @param string $id
	 * @param string $path
	 * @return bool
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function downloadFile( string $id, $path ) {
		$resource = fopen( $path, 'w' );
		if ( !$resource ) {
			$this->getLogger()->error( 'Cannot open resource on ' . $path );
			return false;
		}
		$response = $this->runAuthenticatedRequest(
			'GET', 'model_document_download/' . $id, [], [ 'sink' => $resource ]
		);
		if ( $response->getStatusCode() !== 200 ) {
			$this->getLogger()->error( 'Could not retrieve file resource ' . $id, [
				'error' => $response->getBody()->getContents(),
				'path' => $path,
				'code' => $response->getStatusCode()
			] );
			return false;
		}
		fclose( $resource );
		if ( !file_exists( $path ) ) {
			$this->getLogger()->error( 'Could not download resource ' . $id );
			return false;
		}

		return true;
	}

	/**
	 * @param string $type
	 * @param array $fields
	 * @param array $relations
	 * @param DateTime $from
	 * @return FluxxEntity[]|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function getEntities( string $type, array $fields, array $relations, DateTime $from ): ?array {
		// DEV ONLY - remove changing of $from
		// $from = $from->sub( new \DateInterval( 'P10D' ) );
		$this->getLogger()->info( 'Getting entities for conditions', [
			'field' => $this->getQualifyingFieldForEntityType( $type ),
			'comparison' => 'gt',
			'value' => $from->format( 'Y-m-d\TH:i:s\Z' )
		] );
		$entities = [];
		// Retrieve in batches as long as there are items
		$status = $this->retrieveRecursively( [
			'cols' => json_encode( $fields ),
			'relation' => json_encode( $relations ),
			'sort' => json_encode( [ $this->getQualifyingFieldForEntityType( $type ), 'asc' ] ),
			'filter' => json_encode( [
				[ $this->getQualifyingFieldForEntityType( $type ), 'gt', $from->format( 'Y-m-d\TH:i:s\Z' ) ],
				// DEV ONLY
				// [ 'id', 'eq', '22863318' ]
			] ),
		], $this->getEndpointForEntityType( $type ), $type, $entities );

		$this->getLogger()->info( "Retrieved " . count( $entities ) . " entity(ies)" );
		if ( !$status ) {
			return null;
		}
		return $entities;
	}

	/**
	 * @param string $type
	 * @return string
	 * @throws \Exception
	 */
	private function getEndpointForEntityType( string $type ): string {
		return $this->getEntityMapItem( 'endpoint', $type );
	}

	/**
	 * @param string $type
	 * @return string
	 * @throws \Exception
	 */
	private function getQualifyingFieldForEntityType( string $type ): string {
		return $this->getEntityMapItem( 'qualifying_field', $type );
	}

	/**
	 * @param string $item
	 * @param string $type
	 * @return string
	 * @throws \Exception
	 */
	private function getEntityMapItem( string $item, string $type ): string {
		$value = $this->entityEndpointMap[$type][$item] ?? null;
		if ( !$value ) {
			throw new \Exception( "$item for type $type not defined!" );
		}
		return $value;
	}

	/**
	 * @param array $data
	 * @param string $path
	 * @param string $entityName
	 * @param array &$entities
	 * @param int|null $page
	 * @return bool
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function retrieveRecursively(
		array $data, string $path, string $entityName, array &$entities, ?int $page = 1
	): bool {
		$data['page'] = $page;
		$response = $this->runAuthenticatedRequest( 'POST', $path, $data );
		if ( $response->getStatusCode() !== 200 ) {
			$this->getLogger()->critical( 'Could not retrieve ' . $entityName, [
				'error' => $response->getBody()
			] );
			return false;
		}
		$body = $response->getBody();
		$parsed = json_decode( $body, 1 );
		if ( isset( $parsed['error'] ) ) {
			$this->getLogger()->critical( 'Fluxx response error', $parsed );
		}
		if ( !isset( $parsed['records'][$entityName] ) ) {
			return false;
		}

		foreach ( $parsed['records'][$entityName] as $record ) {
			$entities[] = new FluxxEntity( (array)$record );
		}

		if ( (int)$parsed['total_pages'] > $page ) {
			$status = $this->retrieveRecursively( $data, $path, $entityName, $entities, $page + 1 );
			if ( !$status ) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Retrieve access token
	 *
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function authenticate() {
		$response = $this->request( 'POST', 'oauth/token', [
			'form_params' => [
				'grant_type' => 'client_credentials',
				'client_id' => $this->connectionConfig['client_id'],
				'client_secret' => $this->connectionConfig['client_secret'],
			]
		] );
		$body = json_decode( $response->getBody(), 1 );

		if ( $body === null || !isset( $body['access_token' ] ) ) {
			throw new \RuntimeException( 'Could not retrieve access token from Fluxx' );
		}

		$this->accessToken = $body['access_token'];
	}

	/**
	 * @param string $method
	 * @param string $path
	 * @param array|null $data
	 * @param array|null $options
	 * @return ResponseInterface
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function runAuthenticatedRequest( $method, $path, $data = [], $options = [] ): ResponseInterface {
		if ( !$this->accessToken ) {
			$this->authenticate();
		}

		$path = 'api/rest/v2/' . ltrim( $path, '/' );
		if ( !isset( $options['headers'] ) ) {
			$options['headers'] = [];
		}
		$options['headers']['Authorization'] = "Bearer {$this->accessToken}";
		if ( $method === 'POST' && !empty( $data ) ) {
			$options['form_params'] = $data;
		}
		if ( $method === 'GET' && !empty( $data ) ) {
			$path = $this->compileURL( $path, $data );
		}

		return $this->request( $method, $path, $options );
	}
}
