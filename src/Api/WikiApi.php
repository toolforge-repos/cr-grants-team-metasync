<?php
namespace WikimediaCRGrantsTeam\Tool\MetaSync\Api;

use GuzzleHttp\Psr7\Utils;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use WikimediaCRGrantsTeam\Tool\MetaSync\File;

class WikiApi extends GenericApi {
	/** @var array */
	private $tokens = [];
	/** @var bool */
	private $authenticated = false;

	/**
	 * @param string $title
	 * @param string $content
	 * @param string $summary
	 * @return bool
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function editPage( $title, $content, $summary = '' ): bool {
		$response = $this->postWithToken( 'csrf', [
			'action' => 'edit',
			'title' => $title,
			'text' => $content,
			'summary' => $summary
		] );

		$decoded = json_decode( $response->getBody(), 1 );
		if ( !isset( $decoded['edit']['result'] ) || $decoded['edit']['result'] !== 'Success' ) {
			$this->getLogger()->error(
				'Failed to edit wiki page ' . $title,
				[ 'response' => $decoded ]
			);
			return false;
		}

		$message = 'Edit complete';
		if ( isset( $decoded['edit']['nochange'] ) ) {
			$message = 'Already up to date';
		}
		$this->getLogger()->info( $message, [
			'title' => $decoded['edit']['title'],
			'id' => $decoded['edit']['pageid'],
		] );

		return true;
	}

	/**
	 * @param File $file
	 * @return string|bool if falsed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function uploadFile( File $file ) {
		if ( !$this->authenticated ) {
			$this->authenticate();
		}

		$response = $this->request( 'POST', '', [
			'multipart' => [
				[
					'Content-type' => 'multipart/form-data',
					'name' => 'file',
					'contents' => Utils::tryFopen( $file->getPath(), 'r' ),
					'filename' => $file->getFilename(),
				],
				[ 'name' => 'action', 'contents' => 'upload' ],
				[ 'name' => 'filename', 'contents' => $file->getFilename(), ],
				[ 'name' => 'text', 'contents' => $file->getFilePageContent(), ],
				[ 'name' => 'ignorewarnings', 'contents' => 1, ],
				[ 'name' => 'token', 'contents' => $this->getToken( 'csrf' ) ],
				[ 'name' => 'format', 'contents' => 'json' ],
			],
		] );
		$responseData = json_decode( $response->getBody()->getContents(), 1 );
		if ( !$responseData ) {
			$this->getLogger()->error( 'Failed to upload file ' . $file->getFilename(), [
				'error' => 'Cannot decode response'
			] );
			return false;
		}
		if ( isset( $responseData['error'] ) ) {
			if ( $responseData['error']['code'] === 'fileexists-no-change' ) {
				return true;
			}
			$this->getLogger()->error( 'Failed to upload file ' . $file->getFilename(), [
				'error' => $responseData['error'],
			] );
			return false;
		}
		if ( isset( $responseData['upload'] ) && $responseData['upload']['result'] !== 'Success' ) {
			$this->getLogger()->error( 'Failed to upload file ' . $file->getFilename(), [
				'error' => 'Result not Success',
				'response' => $responseData
			] );
			return false;
		}

		return $responseData['upload']['filename'];
	}

	/**
	 * Get EntityTemplate
	 *
	 * @param string $templateTitle
	 * @return string|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function retrieveTemplateContent( $templateTitle ): ?string {
		return $this->retrievePageContent( $templateTitle );
	}

	/**
	 * @param string $page
	 * @return array|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function retrieveWikiConfig( $page ): ?array {
		$response = $this->retrievePageContent( $page );
		if ( $response === null ) {
			return null;
		}
		$parsed = json_decode( $response, 1 );
		if ( is_array( $parsed ) ) {
			return $parsed;
		}

		return null;
	}

	/**
	 * @param string $page
	 * @return string|null
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	private function retrievePageContent( $page ): ?string {
		$page = ucfirst( str_replace( ' ', '_', $page ) );
		$response = $this->runAuthenticatedRequest( 'GET', '', [
			'action' => 'parse',
			'prop' => 'wikitext',
			'page' => $page
		] );

		$decoded = json_decode( $response->getBody(), 1 );
		if ( isset( $decoded['error'] ) ) {
			$this->getLogger()->error( 'Failed to retrieve wikipage content', [
				'error' => $decoded['error']['info'] ?? $decoded['error']['code'],
				'page' => $page
			] );
			return null;
		}
		if ( isset( $decoded['parse']['wikitext']['*'] ) ) {
			return $decoded['parse']['wikitext']['*'];
		}

		$this->getLogger()->error( 'Failed to retrieve wikipage content', [
			'page' => $page,
		] );
		return null;
	}

	protected function authenticate() {
		$this->getToken( 'login', false );
		$this->doLogin();
	}

	/**
	 * @param string $method
	 * @param string $path
	 * @param array $data
	 * @param array $options
	 * @return ResponseInterface
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	protected function runAuthenticatedRequest( $method, $path, $data = [], $options = [] ): ResponseInterface {
		if ( !$this->authenticated ) {
			$this->authenticate();
		}
		$data['format'] = 'json';

		if ( $method === 'POST' && !empty( $data ) ) {
			$options['form_params'] = $data;
		}
		if ( $method === 'GET' && !empty( $data ) ) {
			$path = $this->compileURL( $path, $data );
		}

		return $this->request( $method, $path, $options );
	}

	/**
	 * @param string $token Token name
	 * @param array $data
	 * @param array $options
	 * @return ResponseInterface
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	private function postWithToken( $token, $data = [], $options = [] ): ResponseInterface {
		if ( !$this->authenticated ) {
			$this->authenticate();
		}
		$data['token'] = $this->getToken( $token );
		return $this->runAuthenticatedRequest( 'POST', '', $data, $options );
	}

	private function doLogin() {
		$response = $this->request( 'POST', '', [
			'form_params' => [
				'action' => 'login',
				'lgname' => $this->connectionConfig['user'],
				'lgpassword' => $this->connectionConfig['password'],
				'lgtoken' => $this->tokens['login'] ?? '',
				'format' => 'json'
			],
		] );
		$responseData = json_decode( $response->getBody(), 1 );
		if (
			isset( $responseData['login']['result'] ) &&
			$responseData['login']['result'] === 'Success'
		) {
			$this->authenticated = true;
			return;
		}
		$this->getLogger()->error( 'Failed to login', [
			'error' => $responseData['login']['result'] ?? 'Unknown',
			'response' => $responseData
		] );
	}

	/**
	 * @param string $name
	 * @param bool $requireAuthentication
	 * @param bool|null $refresh Whether to re-get the token, even if already set
	 * @return mixed
	 */
	private function getToken( $name, $requireAuthentication = true, $refresh = false ) {
		if ( $requireAuthentication && !$this->authenticated ) {
			$this->authenticate();
		}
		$name = strtolower( $name );
		if ( !isset( $this->tokens[$name] ) || $refresh ) {
			$this->setToken( $name );
		}

		return $this->tokens[$name];
	}

	/**
	 * @param string $name
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	private function setToken( $name ) {
		$requestData = [
			'action' => 'query',
			'meta' => 'tokens',
			'type' => $name,
			'format' => 'json'
		];

		$tokenResponseKey = "{$name}token";
		$response = $this->request( 'GET', $this->compileURL( '', $requestData ) );
		$responseData = json_decode( $response->getBody(), 1 );
		if ( !isset( $responseData['query']['tokens'][$tokenResponseKey] ) ) {
			$this->getLogger()->critical( "Could not retrieve token", [
				'token' => $name,
				'responseData' => $responseData,
				'status' => $response->getStatusCode()
			]
		 );
			throw new RuntimeException( "Could not retrieve \"$name\" token" );
		}

		$this->tokens[$name] = $responseData['query']['tokens'][$tokenResponseKey];
	}
}
