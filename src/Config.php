<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use WikimediaCRGrantsTeam\Tool\MetaSync\Api\WikiApi;

class Config {
	/** @var array */
	private $config = null;
	/** @var LoggerInterface */
	private $logger;
	/** @var null */
	private $integrityCheckResult = null;
	/** @var string[] */
	private $schema = [
		'source-grants-management-system-base-url' => 'string',
		'source-grants-management-system-oauth-id' => 'string',
		'source-grants-management-system-oauth-secret' => 'string',
		'target-wiki-url' => 'string',
		'target-wiki-botuser' => 'string',
		'target-wiki-botpassword' => 'string',
		'wiki-config-page' => 'string',
		'data-dir' => 'string',
		'wiki-templates' => 'array'
	];

	/**
	 * @param string $file
	 * @param LoggerInterface $logger
	 */
	public function __construct( $file, LoggerInterface $logger ) {
		$this->logger = $logger;
		if ( !is_readable( $file ) ) {
			$logger->critical( 'Connection config file {file} is not readable', [
				'file' => $file
			] );
			throw new InvalidArgumentException( 'Config cannot be read' );
		}
		$content = file_get_contents( $file );
		$this->config = json_decode( $content, 1 );
		if ( !is_array( $this->config ) ) {
			throw new InvalidArgumentException( 'Config failed to parse' );
		}
	}

	/**
	 * Load configuration from the target wiki
	 *
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function loadWikiConfig() {
		$api = new WikiApi( $this->getWikiConnectionParams(), $this->logger );
		$configPage = $this->config['wiki-config-page'] ?? null;
		if ( !$configPage ) {
			$this->logger->critical(
				'Cannot retrieve wiki config: Config page not configured or not valid.' .
				'Check "wiki-config-page" param in the config file'
			);
			throw new InvalidArgumentException(
				'Config is missing "wiki-config-page" key, or set to invalid value'
			);
		}
		$data = $api->retrieveWikiConfig( $configPage );
		if ( $data === null ) {
			$this->logger->critical(
				'Cannot retrieve wiki config: Could not read config page', [
					'page' => $configPage,
				]
			);
			throw new Exception(
				'Cannot retrieve wiki config: Could not read config page'
			);
		}
		$this->config = array_merge( $this->config, $data );
	}

	/**
	 * @return bool
	 */
	public function validateIntegrity() {
		if ( $this->integrityCheckResult === null ) {
			foreach ( $this->schema as $field => $type ) {
				if ( !isset( $this->config[$field] ) ) {
					$this->integrityCheckResult = false;
					throw new InvalidArgumentException( "Config variable $field is not set" );
				}
				if ( gettype( $this->config[$field] ) !== $type ) {
					$this->integrityCheckResult = false;
					throw new InvalidArgumentException( "Config variable $field must be set to type $type" );
				}
			}
			$this->integrityCheckResult = true;
		}

		return $this->integrityCheckResult;
	}

	/**
	 * @return array
	 */
	public function getFluxxConnectionParams(): array {
		return [
			'url' => $this->config['source-grants-management-system-base-url'],
			'client_id' => $this->config['source-grants-management-system-oauth-id'],
			'client_secret' => $this->config['source-grants-management-system-oauth-secret'],
		];
	}

	/**
	 * @return array
	 */
	public function getWikiConnectionParams(): array {
		return [
			'url' => $this->config['target-wiki-url'],
			'user' => $this->config['target-wiki-botuser'],
			'password' => $this->config['target-wiki-botpassword'],
		];
	}

	/**
	 * @return array
	 */
	public function getWikiTemplateKeys(): array {
		return isset( $this->config['wiki-templates'] ) ?
			array_keys( $this->config['wiki-templates'] ) : [];
	}

	/**
	 * @param string $name
	 * @return array|null
	 */
	public function getWikiTemplate( string $name ): ?array {
		return $this->config['wiki-templates'][$name] ?? null;
	}

	/**
	 * @return int
	 */
	public function getRetryAttemptLimit(): int {
		return $this->get( 'retry-attempt-limit', 3 );
	}

	/**
	 * @return array|null
	 */
	public function getGoogleDriveConfig(): array {
		return $this->get( 'google-drive-api' );
	}

	/**
	 * @param string $var
	 * @param mixed|null $default
	 * @return mixed|null
	 */
	public function get( $var, $default = null ) {
		return $this->config[$var] ?? $default;
	}

	/**
	 * @return array
	 */
	public function getSyntaxConverters(): array {
		return $this->config['syntax-converters'] ?? [];
	}
}
