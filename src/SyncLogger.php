<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Monolog\Logger;

class SyncLogger extends Logger {
	/** @var ErrorReportMailer|null */
	private $mailer;

	/**
	 * @param string|\Stringable $message
	 * @param array $context
	 */
	public function error( $message, array $context = [] ): void {
		parent::error( $message, $context );
		if ( $this->mailer ) {
			$this->mailer->addError( $message, $context );
		}
	}

	/**
	 * @param string|\Stringable $message
	 * @param array $context
	 */
	public function critical( $message, array $context = [] ): void {
		parent::critical( $message, $context );
		if ( $this->mailer ) {
			$this->mailer->addError( $message, $context );
		}
	}

	/**
	 * @param ErrorReportMailer $mailer
	 */
	public function setMailer( $mailer ) {
		$this->mailer = $mailer;
		$this->mailer->setLogger( $this );
	}
}
