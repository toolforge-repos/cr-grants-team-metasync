<?php

namespace WikimediaCRGrantsTeam\Tool\MetaSync;

use Psr\Log\LoggerInterface;

class EntityTemplate {
	/** @var string */
	private $key;
	/** @var string */
	private $content;
	/** @var string */
	private $titleMask;
	/** @var string */
	private $entityType;
	/** @var array */
	private $conditions;
	/** @var array */
	private $fileLabels;
	/** @var ValueProcessor */
	private $valueProcessor;
	/** @var array|null */
	private $variables = null;
	/** @var array */
	private $relations = [];
	/** @var array */
	private $titleFields = null;
	/** @var array */
	private $fileLists = [];

	/**
	 * @param string $key
	 * @param string $content
	 * @param string $titleMask
	 * @param string $entityType
	 * @param array $conditions
	 * @param array $fileLabels
	 * @param ValueProcessor $valueProcessor
	 */
	public function __construct(
		string $key, string $content, string $titleMask,
		string $entityType, array $conditions, $fileLabels, ValueProcessor $valueProcessor
	) {
		$this->key = $key;
		$this->content = $content;
		$this->titleMask = $titleMask;
		$this->entityType = $entityType;
		$this->conditions = $conditions;
		$this->fileLabels = $fileLabels;
		$this->valueProcessor = $valueProcessor;
	}

	/**
	 * @return string
	 */
	public function getKey(): string {
		return $this->key;
	}

	/**
	 * @return string
	 */
	public function getEntityType(): string {
		return $this->entityType;
	}

	/**
	 * @return array
	 */
	public function getVariables(): array {
		$this->parseTemplateVariables();
		$this->parseTitleFields();
		$all = array_merge(
			[], array_keys( $this->variables ), $this->titleFields,
			$this->getConditionFields(), [ 'model_document_ids' ]
		);
		$all = array_unique( $all );
		return array_values( array_filter( $all, static function ( $var ) {
			return strpos( $var, '.' ) === false;
		} ) );
	}

	/**
	 * @return array
	 */
	public function getRelations(): array {
		$this->parseTemplateVariables();
		$this->parseTitleFields();
		$conditions = $this->conditions;
		if ( !isset( $conditions['include'] ) ) {
			$conditions = [ 'include' => $conditions ];
		}
		foreach ( $conditions as $group => $values ) {
			foreach ( $values as $key => $value ) {
				$this->extractRelations( $key );
			}
		}
		return $this->relations;
	}

	/**
	 * @return array
	 */
	public function getConditions(): array {
		return $this->conditions;
	}

	private function parseTemplateVariables() {
		if ( $this->variables === null ) {
			$this->variables = [];
			$matches = [];
			preg_match_all( '/\$\$(.*?)\$\$/', $this->content, $matches );
			foreach ( $matches[1] as $i => $match ) {
				if ( strpos( $match, '|' ) !== false ) {
					error_log( "INVALID VARIABLE, use semicolon instead of pipe" );
				}
				$bits = explode( ';', $match );
				$variable = array_shift( $bits );
				if ( $variable === 'file_list' ) {
					$type = isset( $bits[0] ) ? $bits[0] : null;
					if ( !$type ) {
						$this->fileLists['_all'] = $matches[0][$i];
					} else {
						$this->fileLists[$type] = $matches[0][$i];
					}
					continue;
				}
				$data = [
					// Used to replace placeholder with actual value
					'placeholder' => $matches[0][$i],
					'default' => $bits[0] ?? '',
					'type' => isset( $bits[1] ) ? $bits[1] : 'text',
				];
				if ( !$this->extractRelations( $variable ) ) {
					continue;
				}
				if ( !isset( $this->variables[$variable] ) ) {
					$this->variables[$variable] = [];
				}
				$this->variables[$variable][] = $data;
			}
		}
	}

	/**
	 * @param string $variable
	 * @return bool
	 */
	private function extractRelations( $variable ) {
		if ( strpos( $variable, '.' ) === false ) {
			return true;
		}
		$relations = explode( '.', $variable );
		if ( count( $relations ) > 2 ) {
			// From API docu: Embedded relations are not currently supported
			return false;
		}
		$relationKey = array_shift( $relations );
		if ( !isset( $this->relations[$relationKey] ) ) {
			$this->relations[$relationKey] = [];
		}
		$field = array_shift( $relations );
		if ( !in_array( $field, $this->relations[$relationKey] ) ) {
			$this->relations[$relationKey][] = $field;
		}
		return true;
	}

	/**
	 * @param FluxxEntity $entity
	 * @param array|null $files
	 * @return string
	 */
	public function process( FluxxEntity $entity, $files = [] ): string {
		$processed = $this->content;
		$this->parseTemplateVariables();
		$replaced = [];
		foreach ( $entity->getAll() as $name => $value ) {
			if ( !isset( $this->variables[$name] ) ) {
				continue;
			}
			$configs = $this->variables[$name];
			foreach ( $configs as $config ) {
				$processedValue = $this->valueProcessor->processValue( $config, $value );
				$replaced[] = $name;
				$processed = str_replace( $config['placeholder'], $processedValue, $processed );
			}
		}
		// Add file lists
		foreach ( $this->fileLists as $type => $ph ) {
			$all = [];
			if ( $type === '_all' ) {
				foreach ( $files as $label => $fileDescriptions ) {
					$all = array_merge( $all, $fileDescriptions );
				}
			} elseif ( isset( $files[$type] ) ) {
				$all = $files[$type];
			}

			$filelist = [];
			foreach ( $all as $fileDescription ) {
				$filelist[] = "* [{$fileDescription['link']} {$fileDescription['name']}]";
			}
			$processed = str_replace( $ph, implode( "\n", $filelist ), $processed );
		}

		// Replace all placeholders that didnt receive value with empty string or specified default
		foreach ( array_diff( array_keys( $this->variables ), $replaced ) as $missing ) {
			foreach ( $this->variables[$missing] as $config ) {
				$placeholder = $config['placeholder'];
				$default = $config['default'];
				$processed = str_replace( $placeholder, $default, $processed );
			}
		}

		return $processed;
	}

	/**
	 * @param FluxxEntity $entity
	 * @param LoggerInterface $logger
	 * @return string|null
	 */
	public function getWikiPageName( FluxxEntity $entity, LoggerInterface $logger ): ?string {
		$title = $this->titleMask;
		$this->parseTitleFields();
		$missingFields = [];
		foreach ( $this->titleFields as $field ) {
			$value = $entity->get( $field, null );
			if ( $value === null ) {
				$missingFields[] = $field;
				$value = '';
			}
			$value = $this->normalizeForWikiTitle( $value );
			$title = preg_replace( '/{' . $field . '\}/', $value, $title );
		}

		if ( !empty( $missingFields ) ) {
			$logger->critical( 'Title mask cannot be converted to a valid title, fields missing', [
				'mask' => $this->titleMask,
				'missing_fields' => json_encode( $missingFields )
			] );
			return null;
		}
		return trim( $title );
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function normalizeForWikiTitle( string $value ) {
		$value = preg_replace( '/\:\s/', ':', $value );
		$value = preg_replace( '/\n/', ' ', $value );
		return $value;
	}

	private function parseTitleFields() {
		if ( $this->titleFields === null ) {
			$matches = [];
			preg_match_all( '/\{(.*?)\}/', $this->titleMask, $matches );

			$this->titleFields = [];
			foreach ( $matches[1] as $field ) {
				if ( !$this->extractRelations( $field ) ) {
					continue;
				}
				$this->titleFields[] = $field;
			}
		}
	}

	/**
	 * @return array
	 */
	public function getFileLabels(): array {
		return $this->fileLabels;
	}

	/**
	 * @return array
	 */
	private function getConditionFields(): array {
		$conditions = $this->conditions;
		if ( !isset( $conditions['include'] ) ) {
			$conditions = [ 'include' => $conditions ];
		}
		$fields = [];
		foreach ( $conditions as $group => $values ) {
			foreach ( $values as $key => $value ) {
				if ( strpos( $key, '.' ) === false ) {
					// If it has a dot, it's a relation
					$fields[] = $key;
				}
			}
		}
		return $fields;
	}

}
